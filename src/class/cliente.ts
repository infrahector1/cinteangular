export class Cliente {
    
    id:number;
    tipoRegistro:String;
    modalidadPlanilla:String;
    secuencia:String;
    tipoDocumento:String;
    nroIdentificacionDocumento:String;
    tipoCotizante:String;
    subtipoCotizante:String;
    extranjeroCotizante:String;
    colombianoResExterior:String;
    codigoDepartamento:String;
    codigoMunicipio:String;
    primerApellido:String;
    segundoApellido:String;
    primerNombre:String;
    segundoNombre:String;
}
