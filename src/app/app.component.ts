import { HttpClient, HttpEventType } from '@angular/common/http';
import { Component,OnInit } from '@angular/core';
import { Cliente } from 'src/class/cliente';
import { Observable,Subject } from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  constructor(private httpClient: HttpClient) { }
  selectedFile: File;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  message: string;
  imageName: any;
  
  empleadosArray: any[] = [];  
  dtOptions: DataTables.Settings = {};  
  dtTrigger: Subject<any>= new Subject();  

  clientes: Observable<Cliente[]>;  
  cliente : Cliente=new Cliente();  
  deleteMessage=false;  
  clientelist:any;  
  isupdated = false;


  ngOnInit(): void {
    this.isupdated=false;  
    this.dtOptions = {  
      pageLength: 6,  
      stateSave:true,  
      lengthMenu:[[6, 16, 20, -1], [6, 16, 20, "All"]],  
      processing: true 

    };
    this.getClientesList().subscribe(data =>{  
      this.clientes =data;  
      this.dtTrigger.next();  
      });
}

  getClientesList(): Observable<any> {  
    return this.httpClient.get('http://localhost:8080/api/clientes_detalle',{
    responseType: 'json' });  
  }

  public onFileChanged(event) {
    this.selectedFile = event.target.files[0];
  }

  onUpload() {
    console.log(this.selectedFile);

    const uploadImageData = new FormData();
    uploadImageData.append('imageFile', this.selectedFile, this.selectedFile.name);

    this.httpClient.post('http://localhost:8080/api/upload', uploadImageData, { observe: 'response' })
      .subscribe((response) => {
        if (response.status === 200) {
          this.isupdated=true;  
          this.getClientesList().subscribe(data =>{  
            this.clientes =data;  
          });
          this.message = 'Archivo subio correctamente';
        } else {
          this.message = 'Error al subir el archivo';
        }
      }
      );
  }
}
